﻿// JScript File

function modalWindow(url, name)
{
    //ShowModalDialog is only available in IE, not firefox. 
   if (window.showModalDialog) 
   {
        window.showModalDialog(url,name,
        "dialogWidth:255px;dialogHeight:250px");
   } 
   else 
   {
        window.open(url, name, 
        'height=255,width=250,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,modal=yes');
   }

}
var windowImage="";
function openImagePicker(htmlFormElementId, imageDir, buttonId)
{
    if (buttonId == undefined)
    {
         var iUrl = "Admin/ImagePicker2.aspx?ReturnMethod=FormElement&Element=" + htmlFormElementId + "&TargetFolder=" + imageDir; 
    }
    else
    {   
         var iUrl = "Admin/ImagePicker2.aspx?ReturnMethod=FormElement&Element=" + htmlFormElementId + "&TargetFolder=" + imageDir + "&Button=" +buttonId; 
    }
    if(windowImage.closed == false){
    windowImage.focus();    
    }else{
    windowImage = window.open(iUrl ,"ImagePicker","toolbar=no,status=no,scrollbars=no,resizable=yes,width=650,height=500");
    } 
}

//HeaderImage Java

function ShowDiv(DivName)
    {
     var mDiv = document.getElementById(DivName);
     //alert(DivName); 
     var str = "" +window.location;
     var pos=str.indexOf("PageCache")
        if (pos>=0)
        {
            mDiv.style.visibility = "visible";
        }    
        
    }
     function CloseDiv(DivName)
    {
     var mDiv = document.getElementById(DivName);
     //alert(div); 
     mDiv.style.visibility = "hidden";   
    }
    var windowImage="";
   function openImagePicker2(htmlFormElementId, imageDir, buttonId)
    {
        if (buttonId == undefined)
        {
             var iUrl = "../ImagePicker2.aspx?ReturnMethod=FormElement&Element=" + htmlFormElementId + "&TargetFolder=" + imageDir; 
        }
        else
        {   
             var iUrl = "../ImagePicker2.aspx?ReturnMethod=FormElement&Element=" + htmlFormElementId + "&TargetFolder=" + imageDir + "&Button=" +buttonId; 
        }
        if(windowImage.closed == false){
        windowImage.focus();    
        }else{
        windowImage = window.open(iUrl ,"ImagePicker","toolbar=no,status=no,scrollbars=no,resizable=yes,width=650,height=500");
        } 
    }

function allValidatorsValid()
{
    //if there are no validators on the page, return true.
    if (Page_Validators == null)
    {   
       return true;
    }
    //go through validators
    var isValid = true;
    for (var i=0; i<Page_Validators.length; i++)
    {
        var validator = Page_Validators[i];
        if (!validator.isvalid)
        {
            alert("invalid");
            var validateControl = document.getElementById(validator.controltovalidate);
            validateControl.className = "invalidTD";
            
            var validatorTD = validateControl.parentElement;
            validatorTD.className = "invalidTD";
            
            isValid = false;
        }
    }
    return isValid;
}

function preloadImages(input){
    if (input.length>0){
        if(input.indexOf(",")>0){
        ImgItems = input.split(",");
        alert(ImgItems)
            for(i=0;i<ImgItems.length;i++){
            preloadThis(ImgItems[i]);
            }
        }
    }else{
        preloadThis(input)
    }
}

function preloadThis(x){
    img = new Image();
    img.src = "images/" + x;
}

function preloadDefaultImages(){
preloadImages("backg.gif","menu,gif","menu_o.gif","menu_bg.gif");
}

function clickButton(e, buttonid,ignore){
var bt = document.getElementById(buttonid)
    if (typeof bt == 'object'){
        if(navigator.appName.indexOf("Netscape")>(-1)){
            if (e.keyCode == 13){ 
                if(ignore){
                    return false
                }else{
                     bt.click()
                     return false
                }
            }
        } 
        if (navigator.appName.indexOf("Microsoft Internet Explorer")>(-1)){
            if (event.keyCode == 13){
                if(ignore){
                    return false
                }else{
                    bt.click()
                    return false
                }
            } 
        } 
     } 
 } 
 
 function addEvent( obj, type, fn ) {
	if (obj.addEventListener) {
		obj.addEventListener( type, fn, false );
	}
	else if (obj.attachEvent) {
		obj["e"+type+fn] = fn;
		obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
		obj.attachEvent( "on"+type, obj[type+fn] );
	}
	else {
		obj["on"+type] = obj["e"+type+fn];
	}
}