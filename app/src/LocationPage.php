<?php

namespace Matrix;

use Page;    
use SilverStripe\Forms\TextField;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\File;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class LocationPage extends Page 
{
	private static $db = [
	    'HtmlContent1' => 'HTMLText',
	    'HtmlContent2' => 'HTMLText',
	];

    private static $has_one = [
        'Photo1' => Image::class,
        'Photo2' => Image::class
    ];

	public function getCMSFields() 
	{
	    $fields = parent::getCMSFields();

	    $fields->removeByName('HtmlContent');
	    $fields->addFieldToTab('Root.Main', UploadField::create('Photo1', 'Photo 1'));	    
	    $fields->addFieldToTab('Root.Main', HTMLEditorField::create('HtmlContent1','Location 1'));
	    $fields->addFieldToTab('Root.Main', UploadField::create('Photo2', 'Photo 2'));
	    $fields->addFieldToTab('Root.Main', HTMLEditorField::create('HtmlContent2','Location 2'));	    

	    return $fields;
	}
}