<?php

namespace Matrix;

use Page;    
use SilverStripe\Forms\TextField;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\File;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldAddExistingAutocompleter;
use SilverStripe\Forms\GridField\GridFieldDataColumns;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\GridField\GridFieldToolbarHeader;
use SilverStripe\Forms\GridField\GridFieldDetailForm;
use SilverStripe\Forms\GridField\GridFieldEditButton;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripe\Forms\GridField\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridFieldConfig;
use SilverStripe\ORM\DataObject;


class PageLinks extends DataObject 
{

	private static $db = array(
		'Title' => 'Varchar',
		'Excerpt' => 'Text',
		'Link' => 'Varchar'
	);

  private static $has_one = array(
      'Pages' => Page::class
  );


	public function getCMSFields() 
	{
	    $fields = parent::getCMSFields();

		// $config = GridFieldConfig_RelationEditor::create();
		
	 //     // Set the girdfield columns
  //       $dataColumns = new GridFieldDataColumns();
  //       $dataColumns->setDisplayFields(
  //           [
  //               'Title'         => 'Title',
  //               'singular_name' => 'Type'
  //           ]
  //       );

  //       // Get available strips from 'strips' folder

  //       // Set up autocompleter to add existing strips
  //       $autocompleter = new GridFieldAddExistingAutocompleter('toolbar-header-right', ['Title']);
  //       $autocompleter->setResultsFormat('$Title ($ClassName)');

        // $stripsGridField = GridField::create('Pages', 'Page Links', $this->Pages(),
        //     GridFieldConfig::create()->addComponents(
        //         new GridFieldToolbarHeader(),
        //         new GridFieldDetailForm(),
        //         new GridFieldEditButton(),
        //         new GridFieldDeleteAction('unlinkrelation'),
        //         // new GridFieldDeleteAction(),                
        //         $autocompleter,
        //         $dataColumns
        //     )
        // );
  //       $stripsGridField->setDescription('IMPORTANT: Always attempt to link an existing page before creating a new one');

  //       $fields->addFieldsToTab(
  //           'Root.Main',
  //           [
  //               $stripsGridField
  //           ]
  //       );
	$field = DropdownField::create('Link', 'Link', Page::get()->map('Link', 'Title'))
        ->setEmptyString('(Select one)');
    $fields->addFieldToTab('Root.Main', $field);

	    // $fields->addFieldToTab('Root.Main', HTMLEditorField::create('HtmlContent','Content below links'));


	    return $fields;
	}
}