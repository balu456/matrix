<?php

use Page as Page;
use SilverStripe\Dev\BuildTask;
use Matrix\PageWidget;
use Matrix\PageLinks;
use SilverStripe\Control\Director;
use SilverStripe\CMS\Controllers\ContentController;

class PublishImages extends BuildTask {

    protected $title = 'Publish all images';

    protected $description = 'Publish all images';

    protected $enabled = true;

    function run($request) {
        $this->changeStage();
    }

    public function changeStage() {
        // projects
        $pages = Page::get();
        echo $pages->count(). ' pages found<br />';

        $i= 1;
        $theme = new ContentController();
        $currTheme = $theme->ThemeDir();
        foreach($pages as $page) {

            $old = $page->TitleContent;
            $new = str_replace("https://www.matrix.co.nz/","/resources/themes/simple/",$old);
            $page->TitleContent = $new;
            $page->write();
            $page->doPublish();            
            // echo '<hr>';
            echo '<br>'.$new;

        }        

    }

}