<?php

namespace Matrix;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\Form;
use SilverStripe\Control\Email\Email;
use PageController; 
use SilverStripe\SpamProtection\Extension\FormSpamProtectionExtension;   

class ContactPageController extends PageController 
{
	private static $allowed_actions = ['ContactForm'];

    public function ContactForm() 
    { 
        $comment = '';
        if(isset($_GET['cmt'])) {
            $comment = 'I am enquiring about: '.$_GET['cmt'];
        } 
        $fields = new FieldList( 
            TextField::create('FirstName')->setTitle('First Name*'), 
            TextField::create('LastName')->setTitle('Last Name*'), 
            TextField::create('Company')->setTitle('Company*'), 
            EmailField::create('Email')->setTitle('Email Address*'), 
            TextField::create('Phone')->setTitle('Phone*')->setMaxLength(20),             
            TextField::create('Country')->setTitle('Country*'),             
            ReadonlyField::create('Note')->setTitle('Note: Our territory for providing products is limited to Australia/New Zealand, but we are happy to provide services globally.'),             
            TextareaField::create('Message')->setValue($comment)->setTitle('Comments'),
            ReadonlyField::create('Mandatory')->setTitle('All fields marked with * are required')
        ); 
        $submit = new FormAction('submit', 'Submit');
        $submit = $submit->addExtraClass('btn btn-default');
        $actions = new FieldList( 
           $submit
        ); 

        $validator = new RequiredFields('FirstName', 'LastName', 'Company', 'Email', 'Phone', 'Country');
        $form = new Form($this, 'ContactForm', $fields, $actions, $validator); 
        $form->enableSpamProtection()
            ->fields()->fieldByName('Captcha')
            ->setDescription("Please tick the box to prove you're a human and help us stop spam.");

        return $form;
    }

    public function submit($data, $form) 
    { 
        $email = new Email(); 

        $email->setTo('sales@matrix.co.nz'); 
        $email->setFrom($data['Email']); 
        $email->setSubject("Contact Message from {$data["FirstName"]}"); 

        $messageBody = " 
            <p><strong>First Name:</strong> {$data['FirstName']}</p> 
            <p><strong>Last Name:</strong> {$data['LastName']}</p> 
            <p><strong>Company:</strong> {$data['Company']}</p> 
            <p><strong>Email:</strong> {$data['Email']}</p> 
            <p><strong>Phone:</strong> {$data['Phone']}</p> 
            <p><strong>Country:</strong> {$data['Country']}</p> 
            <p><strong>Message:</strong> {$data['Message']}</p> 
        "; 
        $email->setBody($messageBody); 
        $email->send();        
        return [
            'Message' => 'Thank you for contacting us.',
            'ContactForm' => ''
        ];
    }
}