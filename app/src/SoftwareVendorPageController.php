<?php

namespace Matrix;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\Form;
use SilverStripe\Control\Email\Email;
use Dynamic\CountryDropdownField\Fields\CountryDropdownField;
use PageController;    

class SoftwareVendorPageController extends PageController 
{
	private static $allowed_actions = ['SoftwareVendorForm'];

    public function SoftwareVendorForm() 
    { 
        $comment = '';
        if(isset($_GET['cmt'])) {
            $comment = 'I am enquiring about: '.$_GET['cmt'];
        } 
        $fields = new FieldList( 
            TextField::create('Name')->setTitle('Full Name:'), 
            TextField::create('Company')->setTitle('Company:'), 
            EmailField::create('Email')->setTitle('Email:'),  
            ReadonlyField::create('CAE')->addExtraClass('ContentSubHeading'),               
                        
            CheckboxField::create('SimuliaInsight(Abaqus)')->setTitle('Simulia Insight (Abaqus)'),
            CheckboxField::create('CD-Adapco')->setTitle('CD-Adapco (Star-CD)'),
            CheckboxField::create('CSC')->setTitle('CSC (S-Frame & Tedds)'),
            ReadonlyField::create('Infrastructure')->setTitle('Infrastructure Asset Information Management')->addExtraClass('ContentSubHeading'),
            CheckboxField::create('MUNSYS')->setTitle('MUNSYS'),
            CheckboxField::create('AutodeskSpatialNews')->setTitle('Autodesk Spatial News'),
            CheckboxField::create('Prolog(MeridianSystems)')->setTitle('Prolog (Meridian Systems)'), 
            CheckboxField::create('BlueCieloECMSolutions')->setTitle('BlueCielo ECM Solutions'), 
            ReadonlyField::create('Plant')->setTitle('Plant and Process')->addExtraClass('ContentSubHeading'),    
            CheckboxField::create('Intergraph')->setTitle('Intergraph Plant, Process & Marine')
        ); 
        $submit = new FormAction('submit', 'Submit');
        $submit = $submit->addExtraClass('btn btn-default');
        $actions = new FieldList( 
           $submit
        ); 

        $validator = new RequiredFields('Name', 'Company', 'Email');
        $form = new Form($this, 'SoftwareVendorForm', $fields, $actions, $validator);
        $form->enableSpamProtection()
            ->fields()->fieldByName('Captcha')
            ->setDescription("Please tick the box to prove you're a human and help us stop spam.");
        return $form;
 
    }

    public function submit($data, $form) 
    { 
        $email = new Email(); 

        $email->setTo('webmaster@matrix.co.nz'); 
        $email->setFrom($data['Email']); 
        $email->setSubject("New email from Software Vendor Form"); 

        $messageBody = " 
            <p><strong>Name:</strong> {$data['Name']}</p> 
            <p><strong>Company:</strong> {$data['Company']}</p> 
            <p><strong>Email:</strong> {$data['Email']}</p> 
        "; 
        $email->setBody($messageBody); 
        $email->send();        
        return [
            'Message' => 'Thank you for registering your interest.  Your subscription request is important to us.  We have added your details to ensure you recieve the latest edition of the selected newsletter when released. <br />With best regards, <br />The Matrix Team.',
            'SoftwareVendorForm' => ''
        ];
    }
}