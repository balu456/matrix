<?php

namespace {

    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Assets\Image;
	use SilverStripe\Assets\File;
	use SilverStripe\AssetAdmin\Forms\UploadField;	
	use SilverStripe\Forms\TextAreaField;
	use SilverStripe\Forms\HTMLEditor\HTMLEditorField;		
	use Matrix\PageLinks;
	use SilverStripe\Forms\GridField\GridField;
	use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
	use Matrix\PageWidget;

    class Page extends SiteTree
    {
        private static $db = [
	    	'TitleContent' => 'HTMLText',
	    	'HtmlContent' => 'HTMLText'
        ];

        private static $has_many = array(
	        'PageLinks'	 => PageLinks::class,
	        'PageWidgets' => PageWidget::class
	    );


	       
	    public function getCMSFields() {

		    $fields = parent::getCMSFields();
		    $pageLinks = GridField::create('Pages', 'Page Links', $this->PageLinks(),
		     GridFieldConfig_RecordEditor::create()
	        );

	        $fields->addFieldToTab('Root.Sidebar', GridField::create(
	            'Widgets',
	            'Widgets on this page',
	            $this->PageWidgets(),
	            GridFieldConfig_RecordEditor::create()
	        ));
		    
		    $fields->addFieldToTab('Root.Main', $pageLinks);
	    	$fields->addFieldToTab('Root.TitleImages', HTMLEditorField::create('TitleContent','Images right to title of the page.'), 'Content');		    
	    	$fields->addFieldToTab('Root.Main', HTMLEditorField::create('HtmlContent','Content below links'));		    


		    return $fields;
		}

    }
}
