<?php

namespace Matrix;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\Form;
use SilverStripe\Control\Email\Email;
use PageController;    

class FeedbackPageController extends PageController 
{
	private static $allowed_actions = ['FeedbackForm'];

    public function FeedbackForm() 
    { 
        $comment = '';
        if(isset($_GET['cmt'])) {
            $comment = 'I am enquiring about: '.$_GET['cmt'];
        } 
        $fields = new FieldList( 
            TextField::create('FirstName')->setTitle('First Name*'), 
            TextField::create('LastName')->setTitle('Last Name*'), 
            TextField::create('Company')->setTitle('Company*'), 
            EmailField::create('Email')->setTitle('Email Address*'), 
            ReadonlyField::create('text1')->setTitle('Please answer the following questions as honestly as possible. We welcome both your negative and positive feedback.'),
            OptionsetField::create( $name = "website", $title = "How user friendly did you find the website? 
(1 = poor, 4 = very good)", $source = array( "1" => "1", "2" => "2", "3" => "3", "4" => "4" ), $value = "1" ),
            OptionsetField::create( $name = "brand", $title = "Do you like the new Matrix brand colours and logo?
(1 = not at all, 4 = very much)", $source = array( "1" => "1", "2" => "2", "3" => "3", "4" => "4" ), $value = "1" ),
        OptionsetField::create( $name = "innovation", $title = "What do you think of the new Matrix slogan 'Engineers of Innovation'?
    (1 = not applicable to Matrix, 4 = highly applicable to Matrix)", $source = array( "1" => "1", "2" => "2", "3" => "3", "4" => "4" ), $value = "1" ),
        OptionsetField::create( $name = "ease-access", $title = "Did you find it easy to navigate around the website?
    (1 = poor, 4 = very easy)", $source = array( "1" => "1", "2" => "2", "3" => "3", "4" => "4" ), $value = "1" ), 
        OptionsetField::create( $name = "solution", $title = "Was the information provided in the Solutions area relevant and useful?
(1 = not at all, 4 = very useful)", $source = array( "1" => "1", "2" => "2", "3" => "3", "4" => "4" ), $value = "1" ),
        OptionsetField::create( $name = "product", $title = "Do you like the way the website is split into Solutions and Product areas?
(1 = not at all, 4 = very much)", $source = array( "1" => "1", "2" => "2", "3" => "3", "4" => "4" ), $value = "1" ),
        TextareaField::create('beloved')->setTitle('What do you like the most about the website?'),
        TextareaField::create('improvements')->setTitle('What could be improved?'),
        TextareaField::create('missing')->setTitle('Did you find any faulty or missing links? Please indicate where.'),
        TextareaField::create('comments')->setTitle('Do you have any further comments?')

        );
        $submit = new FormAction('submit', 'Submit');
        $submit = $submit->addExtraClass('btn btn-default');
        $actions = new FieldList( 
           $submit
        ); 

        $validator = new RequiredFields('FirstName', 'LastName', 'Company', 'Email','beloved','improvements','missing','comments');
        $form = new Form($this, 'FeedbackForm', $fields, $actions, $validator);
        $form->enableSpamProtection()
            ->fields()->fieldByName('Captcha')
            ->setDescription("Please tick the box to prove you're a human and help us stop spam.");
        return $form; 
    }

    public function submit($data, $form) 
    { 
        $email = new Email(); 

        $email->setTo('sales@matrix.co.nz'); 
        $email->setFrom($data['Email']); 
        $email->setSubject("New feedback from {$data["FirstName"]}"); 

        $messageBody = " 
            <p><strong>First Name:</strong> {$data['FirstName']}</p> 
            <p><strong>Last Name:</strong> {$data['LastName']}</p> 
            <p><strong>Company:</strong> {$data['Company']}</p> 
            <p><strong>Email:</strong> {$data['Email']}</p> 
            <p><strong>What do you like the most about the website?:</strong> {$data['beloved']}</p> 
            <p><strong>What could be improved?:</strong> {$data['improvements']}</p> 
            <p><strong>Did you find any faulty or missing links? Please indicate where:</strong> {$data['missing']}</p> 
            <p><strong>Message:</strong> {$data['comments']}</p> 
        "; 
        $email->setBody($messageBody); 
        $email->send();        
        return [
            'Message' => 'Thanks for your feedback',
            'FeedbackForm' => ''
        ];
    }
}