<?php

namespace Matrix;

use SilverStripe\ORM\Filters\PartialMatchFilter;
use SilverStripe\ORM\Filters\GreaterThanFilter;
use SilverStripe\ORM\Search\SearchContext;
use Page;    
use Matrix\News;    
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

class NewsPage extends Page 
{
	private static $db = [
	    'Heading' => 'Varchar',
	    'HtmlContent' => 'HTMLText',
	];

  private static $has_many = [
      'News' => News::class       
  ];

	public function getCMSFields() 
	{
	    $fields = parent::getCMSFields();
	    $fields->removeByName('Content');
	    $news = new GridField(
            'News',
            'Add news',
            $this->News(),
            GridFieldConfig_RecordEditor::create()
        );            
        $fields->addFieldToTab('Root.News', $news);


	    return $fields;
	}
}