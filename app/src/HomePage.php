<?php

namespace Matrix;

use Page;    
use SilverStripe\Forms\TextField;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\File;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\DropdownField;


class HomePage extends Page 
{
	private static $db = [
	    'Heading' => 'Varchar',
	    'PhotoLink1' => 'Varchar',
	    'PhotoLink2' => 'Varchar',
	    'PhotoLink3' => 'Varchar',
	    'HtmlContent' => 'HTMLText',
	];

    private static $has_one = [
        'Photo1' => Image::class,
        'Photo2' => Image::class,
        'Photo3' => Image::class
    ];

	public function getCMSFields() 
	{
	    $fields = parent::getCMSFields();

	    $fields->addFieldToTab('Root.Main', TextField::create('Heading','Heading of page'), 'Content');

	    $field = DropdownField::create('PhotoLink1', 'Photo Link 1', Page::get()->map('Link', 'Title'))
        ->setEmptyString('(Select one)');
        $fields->addFieldToTab('Root.Main', $field, 'Photo1');
	    $fields->addFieldToTab('Root.Main', UploadField::create('Photo1', 'Photo 1'), 'Content');
	   	$field = DropdownField::create('PhotoLink2', 'Photo Link 2', Page::get()->map('Link', 'Title'))
        ->setEmptyString('(Select one)');

        $fields->addFieldToTab('Root.Main', $field, 'Photo2');        
	    $fields->addFieldToTab('Root.Main', UploadField::create('Photo2', 'Photo 2'), 'Content');

	    $field = DropdownField::create('PhotoLink3', 'Photo Link 3', Page::get()->map('Link', 'Title'))
        ->setEmptyString('(Select one)');
        $fields->addFieldToTab('Root.Main', $field, 'Photo3');        
	    $fields->addFieldToTab('Root.Main', UploadField::create('Photo3', 'Photo 3'), 'Content');


	    return $fields;
	}
}