<?php

namespace Matrix;

use Matrix\NewsPage;    
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;

class News extends DataObject
{
	private static $db = array(
		'Title' => 'Varchar',
		'ArchiveTitle' => 'Varchar',
		'HtmlContent' => 'HTMLText'
	);
  	private static $has_one = array(
  		'NewsPage' => NewsPage::class
  	);

    public function Link()
    {
        return $this->NewsPage()->Link('show/'.$this->ID);
    }

    public function getCMSFields() {
    	$fields = parent::getCMSFields();
    	$fields->removeByName('NewsPageID');
	    // $fields->addFieldToTab('Root.Main', T::create('HtmlContent','Content below links'));		        	
	    // $fields->addFieldToTab('Root.Main', HTMLEditorField::create('HtmlContent','Content below links'));		        	

	    return $fields;
    }
}