<?php

namespace Matrix;

use Page;
use SilverStripe\ORM\DataObject;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\File;
// use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
// use SilverStripe\Forms\ListboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Matrix\ThreeColPage;
use Matrix\Gallery;
use SilverStripe\Control\Director;

class PageWidget extends DataObject
{
    private static $db = array(
        'Title' => 'Varchar',
        'HtmlContent' => 'HTMLText'
    );

    private static $has_one = array(
        'Pages' => Page::class        
    );

    // public function onBeforeWrite() 
    // {   
    //     $cleanHtml = str_replace('<br />', ' ', $this->HtmlContent );
    //     $cleanHtml = preg_replace('/<a(.*?)href=(["\'])(.*?)\\2(.*?)>/i','<a$1href="#"$3>',$this->HtmlContent);
    //     $this->HtmlContent = $cleanHtml; 
        
    //     parent::onBeforeWrite();
    // }

    public function getCMSFields()
    {
    	$fields = parent::getCMSFields();
        $fields->removeByName('ThreeColPageID');
        
        $fields->addFieldToTab('Root.Main', HTMLEditorField::create('HtmlContent','Content below links'));

        return $fields;
    }
}