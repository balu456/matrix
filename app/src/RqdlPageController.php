<?php

namespace Matrix;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\Form;
use SilverStripe\Control\Email\Email;
use Dynamic\CountryDropdownField\Fields\CountryDropdownField;
use PageController;    

class RqdlPageController extends PageController 
{
	private static $allowed_actions = ['RqdlContactForm'];

    public function RqdlContactForm() 
    { 
        $comment = '';
        if(isset($_GET['cmt'])) {
            $comment = 'I am enquiring about: '.$_GET['cmt'];
        } 
        $fields = new FieldList( 
            ReadonlyField::create('label')->setTitle('Please complete these fields to download now.'),            
            TextField::create('Name')->setTitle('Your Name:'), 
            TextField::create('Position')->setTitle('Position:'), 
            TextField::create('Company')->setTitle('Company:'), 
            EmailField::create('Email')->setTitle('Email:'), 
            TextField::create('Phone')->setTitle('Phone:')->setMaxLength(20),             
            CountryDropdownField::create('Country')->setValue('nz')->setTitle('Country:'),             
            
            TextField::create('Comments')->setValue($comment)->setTitle('Comments:'),
            ReadonlyField::create('text')->setTitle('Thank You')
        ); 
        $submit = new FormAction('submit', 'Submit');
        $submit = $submit->addExtraClass('btn btn-default');
        $actions = new FieldList( 
           $submit
        ); 

        $validator = new RequiredFields('Name', 'Position', 'Company', 'Email', 'Phone');
        $form = new Form($this, 'RqdlContactForm', $fields, $actions, $validator); 
        $form->enableSpamProtection()
            ->fields()->fieldByName('Captcha')
            ->setDescription("Please tick the box to prove you're a human and help us stop spam.");
        return $form;

    }

    public function submit($data, $form) 
    { 
        $email = new Email(); 

        $email->setTo('sales@matrix.co.nz'); 
        $email->setFrom($data['Email']); 
        $email->setSubject("Software Vendor Newsletters Signup from {$data["Name"]}"); 

        $messageBody = " 
            <p><strong>Name:</strong> {$data['Name']}</p> 
            <p><strong>Position:</strong> {$data['Position']}</p> 
            <p><strong>Company:</strong> {$data['Company']}</p> 
            <p><strong>Email:</strong> {$data['Email']}</p> 
            <p><strong>Phone:</strong> {$data['Phone']}</p> 
            <p><strong>Country:</strong> {$data['Country']}</p> 
            <p><strong>Comments:</strong> {$data['Comments']}</p> 
        "; 
        $email->setBody($messageBody); 
        $email->send();        
        return [
            'Message' => 'Thanks for signing up for newsletters.',
            'RqdlContactForm' => ''
        ];
    }
}