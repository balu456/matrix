<?php

namespace Matrix;

use SilverStripe\Control\HTTPRequest;
use Matrix\News;
use PageController;    

class NewsPageController extends PageController 
{
	private static $allowed_actions = [
        'show'
    ];

    public function show(HTTPRequest $request)
    {
        $news = News::get()->byID($request->param('ID'));

        if(!$news) {
            return $this->httpError(404,'That news could not be found');
        }

        return [
            'News' => $news
        ];
    }
}